﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ObjectInteract : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private float rotateSpeed = 10f;
    [SerializeField] 
    private float scaleSpeed = .05f;

    private Vector3 objectStartScale;
    private Quaternion objectStartRotation;

    float initDistance;
    float currentDistance;

    // Start is called before the first frame update
    void Start()
    {
        objectStartScale = transform.localScale;
        objectStartRotation = transform.rotation;

        GameplayUIManager.OnResetBtnClicked += ResetObject;
    }

    private void OnDestroy()
    {
        GameplayUIManager.OnResetBtnClicked -= ResetObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 1)
        {
            SwipeRotation();
            print("swipe input");
        }

        if (Input.touchCount >= 2)
        {
            PinchScaling();
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            ScaleObject(1);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            ScaleObject(-1);
        }
    }

    private void PinchScaling()
    {
        Touch touch0 = Input.GetTouch(0);
        Touch touch1 = Input.GetTouch(1);

        if (touch0.phase == TouchPhase.Began || touch1.phase == TouchPhase.Began)
        {
            initDistance = Vector2.Distance(touch0.position, touch1.position);
        }

        currentDistance = Vector2.Distance(touch0.position, touch1.position);

        if (initDistance > currentDistance)
            ScaleObject(-1);
        else if (initDistance < currentDistance)
            ScaleObject(1);
    }

    private void SwipeRotation()
    {
        Touch touch0 = Input.GetTouch(0);

        print("Object touched");

        if (touch0.phase == TouchPhase.Moved)
        {
            RotatePlane(-touch0.deltaPosition.x, -touch0.deltaPosition.y);
        }
    }

    private void RotatePlane(float deltaPosX, float deltaPosY)
    {
        Quaternion yRotation = Quaternion.Euler(-deltaPosY * rotateSpeed * Time.deltaTime, -deltaPosX * rotateSpeed * Time.deltaTime, 0f);
        transform.rotation *= yRotation;
    }

    private void ScaleObject(float scaleFactor)
    {
        if (transform.localScale.x <= objectStartScale.x && scaleFactor == -1) return;

        transform.localScale += new Vector3(scaleFactor * scaleSpeed, scaleFactor * scaleSpeed, scaleFactor * scaleSpeed);
    }

    private void ResetObject()
    {
        transform.localScale = objectStartScale;
        transform.rotation = objectStartRotation;
    }

    private void OnMouseDown()
    {
        print("Object clicked");
    }

    public void OnPointerClick(PointerEventData eventData)
    {

    }
}
