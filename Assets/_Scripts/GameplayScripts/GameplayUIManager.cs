﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayUIManager : MonoBehaviour
{
    [SerializeField]
    private AudioClip mainBGM;

    [SerializeField]
    private Button backBtn, resetBtn, sSBtn;

    // Start is called before the first frame update
    private void Start()
    {
        TrackableEventHandlerMod.OnTrackableFound += TrackableFound;
        TrackableEventHandlerMod.OnTrackableLost += TrackableLost;

        HideNShowUIs(false);

        backBtn.onClick.AddListener(ClickedBackBtn);
    }

    private void OnDestroy()
    {
        TrackableEventHandlerMod.OnTrackableFound -= TrackableFound;
        TrackableEventHandlerMod.OnTrackableLost -= TrackableLost;
    }

    void TrackableFound()
    {
        HideNShowUIs(true);
        SoundManager.Instance.PlayMusic(mainBGM);
    }

    void TrackableLost()
    {
        HideNShowUIs(false);
        SoundManager.Instance.StopMusic();
    }

    private void HideNShowUIs(bool isHides)
    {
        resetBtn.gameObject.SetActive(isHides);
        sSBtn.gameObject.SetActive(isHides);
    }

    public static event Action OnResetBtnClicked;
    public void ClickedResetBtn()
    {
        SoundManager.Instance.PlayBtnOneshot();
        OnResetBtnClicked?.Invoke();
    }

    public static event Action OnScreeshotBtnClicked;
    public void ScreenshotBtnBehaviour()
    {
        SoundManager.Instance.PlayBtnOneshot();
        OnScreeshotBtnClicked?.Invoke();
    }

    private void ClickedBackBtn()
    {
        SoundManager.Instance.PlayBtnOneshot();
        SoundManager.Instance.StopMusic();
        SceneManager.LoadScene("MainMenu");
    }
}
