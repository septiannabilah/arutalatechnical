﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class TrackableEventHandlerMod : DefaultTrackableEventHandler
{
    public static event Action OnTrackableFound;
    public static event Action OnTrackableLost;

    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();
        OnTrackableFound?.Invoke();
    }

    protected override void OnTrackingLost()
    {
        base.OnTrackingLost();
        OnTrackableLost?.Invoke();
    }
}
