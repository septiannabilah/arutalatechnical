﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScreenCaptureManager : MonoBehaviour
{
    [SerializeField]
    private GameObject blinkPanel;
    [SerializeField]
    private Text directoryText;

    // Start is called before the first frame update
    void Start()
    {
        GameplayUIManager.OnScreeshotBtnClicked += ScreenCapturing;

        blinkPanel.SetActive(false);
        print("SaveFileDirectory: " + Directory.GetCurrentDirectory());
        directoryText.text = Directory.GetCurrentDirectory();
    }

    private void OnDestroy()
    {
        GameplayUIManager.OnScreeshotBtnClicked -= ScreenCapturing;
    }

    public void ScreenCapturing()
    {
        StartCoroutine(COTakeScreenshotAndSave());
    }

    private IEnumerator COTakeScreenshotAndSave()
    {
        yield return new WaitForEndOfFrame();

        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();

        //Blink the Screen
        blinkPanel.SetActive(true);
        yield return new WaitForSeconds(.3f);
        blinkPanel.SetActive(false);

        // Save the screenshot to Gallery/Photos
        string name = string.Format("{0}_Capture{1}_{2}.png", Application.productName, "{0}", System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
        Debug.Log("Permission result: " + NativeGallery.SaveImageToGallery(ss, Application.productName + " Captures", name));
    }
}
