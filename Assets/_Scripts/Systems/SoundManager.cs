﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
	// Audio players components.
	private AudioSource UISource;
	private AudioSource EffectsSource;
	private AudioSource MusicSource;

	// Singleton instance.
	public static SoundManager Instance = null;

	[SerializeField]
	private ScriptableAudio audioScriptable;

	// Initialize the singleton instance.
	private void Awake()
	{
		// If there is not already an instance of SoundManager, set it to this.
		if (Instance == null)
		{
			Instance = this;
		}
		//If an instance already exists, destroy whatever this object is to enforce the singleton.
		else if (Instance != this)
		{
			Destroy(gameObject);
		}
		//Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad(gameObject);

		PrepareAudioSrc();
	}

	private void PrepareAudioSrc()
    {
		UISource = CreateAudioSource("UI Source").GetComponent<AudioSource>();
		EffectsSource = CreateAudioSource("Effect Source").GetComponent<AudioSource>();
		MusicSource = CreateAudioSource("Music Source").GetComponent<AudioSource>();
	}

	private GameObject CreateAudioSource(string name)
    {
		GameObject go = new GameObject(name, typeof(AudioSource));
		go.transform.SetParent(this.transform);
		return go;
    }

	public void PlayEffect(AudioClip clip, float volume = 1)
	{
		EffectsSource.PlayOneShot(clip, volume);
	}

	public void PlayMusic(AudioClip clip, float volume = 1)
	{
		MusicSource.clip = clip;
		MusicSource.volume = volume;
		MusicSource.Play();
	}

	public void StopMusic()
    {
		MusicSource.Stop();
    }

	public void PlayBtnOneshot(float volume = 1)
    {
		AudioClip clip = audioScriptable.buttonClip;
		UISource.PlayOneShot(clip, volume);
    }
}

