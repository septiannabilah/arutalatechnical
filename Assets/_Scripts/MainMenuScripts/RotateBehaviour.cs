﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBehaviour : MonoBehaviour
{
    [SerializeField]
    private float rotateSpeed = 10;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RotatePlane();
    }

    private void RotatePlane()
    {
        float deltaPosX = -1;
        float deltaPosY = -1;
        Quaternion yRotation = Quaternion.Euler(0, -deltaPosY * rotateSpeed * Time.deltaTime, 0f);
        transform.rotation *= yRotation;
    }
}
