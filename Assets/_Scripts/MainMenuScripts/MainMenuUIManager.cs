﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuUIManager : MonoBehaviour
{
    [SerializeField]
    private Button startBtn, ARBtn, TimeBtn;

    // Start is called before the first frame update
    void Start()
    {
        startBtn.onClick.AddListener(ClickedStartBtn);

        ARBtn.onClick.AddListener(ClickedARBtn);
        TimeBtn.onClick.AddListener(ClickedTimeBtn);

        ARBtn.gameObject.SetActive(false);
        TimeBtn.gameObject.SetActive(false);
    }

    private void ClickedStartBtn()
    {
        SoundManager.Instance.PlayBtnOneshot();
        startBtn.gameObject.SetActive(false);

        ARBtn.gameObject.SetActive(true);
        TimeBtn.gameObject.SetActive(true);
    }

    private void ClickedARBtn()
    {
        SoundManager.Instance.PlayBtnOneshot();
        SceneManager.LoadScene("ARGameplay");
    }

    private void ClickedTimeBtn()
    {
        SoundManager.Instance.PlayBtnOneshot();
        SceneManager.LoadScene("TimeZone");
    }
}
