﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class TimeZone : MonoBehaviour
{
    private string input;
    private string output;

    // Start is called before the first frame update
    void Start()
    {
        TimeConvUIManager.OnTimeInput += TimeConverter;
    }

    private void OnDestroy()
    {
        TimeConvUIManager.OnTimeInput -= TimeConverter;
    }

    public static event Action<string> OnTimeConverted;
    private void TimeConverter(string inputTime)
    {
        input = inputTime;
        DateTime dt = DateTime.Parse(input);
        output = dt.ToString("HH:mm:ss", CultureInfo.CurrentCulture);

        OnTimeConverted?.Invoke(output);
    }
}
