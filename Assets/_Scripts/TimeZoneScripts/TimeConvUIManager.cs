﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimeConvUIManager : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField timeInputField;
    [SerializeField]
    private TMP_Text timeOutputField;

    [SerializeField]
    private Button backBtn;

    // Start is called before the first frame update
    void Start()
    {
        TimeZone.OnTimeConverted += OutputTimeDisplay;

        backBtn.onClick.AddListener(ClickedBackBtn);
    }

    private void OnDestroy()
    {
        TimeZone.OnTimeConverted -= OutputTimeDisplay;
    }

    public static event Action<string> OnTimeInput;
    public void InputTimeConverter()
    {
        SoundManager.Instance.PlayBtnOneshot();
        string timeInput = timeInputField.text;
        OnTimeInput?.Invoke(timeInput);
    }

    private void OutputTimeDisplay(string timeOutput)
    {
        timeOutputField.text = timeOutput;
    }

    private void ClickedBackBtn()
    {
        SoundManager.Instance.PlayBtnOneshot();
        SceneManager.LoadScene("MainMenu");
    }
}
