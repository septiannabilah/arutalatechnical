﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Scriptable Audio")]
public class ScriptableAudio : ScriptableObject
{
    public AudioClip buttonClip;
}
